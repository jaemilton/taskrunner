﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace TaskRunner
{
    public class TaskRunnerHelper
    {

        private static FileInfo psExecTempFileInfo = 
            new FileInfo
            (
                Path.Combine(Path.GetTempPath(), 
                    Environment.Is64BitProcess ? "PsExec64.exe" : "PsExec.exe")
            );
        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private static string GetCommandTaskArgs(string command, string args)
        {
            return $@"-accepteula -sd -i \\{Environment.MachineName} {command} {args}".Trim();
        }

        /// <summary>
        /// Grant full access to file to everyone
        /// </summary>
        /// <param name="fullPath"></param>
        private static void GrantAccess(string fullPath)
        {
            FileInfo dInfo = new FileInfo(fullPath);
            FileSecurity fSecurity = dInfo.GetAccessControl();
            fSecurity.AddAccessRule
            (   new FileSystemAccessRule
                (
                    new SecurityIdentifier(WellKnownSidType.WorldSid, null), 
                    FileSystemRights.FullControl, 
                    InheritanceFlags.None, 
                    PropagationFlags.NoPropagateInherit, 
                    AccessControlType.Allow
                )
            );
            dInfo.SetAccessControl(fSecurity);
        }

        public static void StartProcessesFromCommandsFile(string commandFilesPath)
        {
            //Check
            FileInfo fi = new FileInfo(commandFilesPath);
            if (fi.Exists)
            {
                var commandLines = File.ReadAllLines(fi.FullName);

                if (commandLines != null && commandLines.Length > 0)
                {
                    string tempFile = Path.GetTempFileName();
                    
                    using (var sw = new StreamWriter(tempFile))
                    {

                        foreach (var commandLine in commandLines)
                        {

                            if (!string.IsNullOrEmpty(commandLine))
                            {
                                var commandParts = commandLine.Split(' ');
                                var command = commandParts[0];

                                if (!psExecTempFileInfo.Exists)
                                {
                                    File.WriteAllBytes(psExecTempFileInfo.FullName,
                                         Environment.Is64BitProcess ? Resource.PsExec64 : Resource.PsExec);

                                    psExecTempFileInfo.Refresh();
                                }

                                if (psExecTempFileInfo.Exists)
                                {
                                    // Part 1: use ProcessStartInfo class.
                                    ProcessStartInfo startInfo = new ProcessStartInfo();
                                    startInfo.CreateNoWindow = false;
                                    startInfo.UseShellExecute = false;
                                    startInfo.FileName = psExecTempFileInfo.FullName;
                                    startInfo.WindowStyle = ProcessWindowStyle.Normal;

                                    string args = string.Empty;
                                    // Part 2: set command arguments.
                                    if (commandParts.Length > 1)
                                    {
                                        args = string.Join(" ", commandParts.Skip(1));
                                    }
                                    
                                    startInfo.Arguments = GetCommandTaskArgs(command, args);

                                    try
                                    {
                                        // Part 3: start with the info we specified.
                                        // ... Call WaitForExit.
                                        using (Process exeProcess = Process.Start(startInfo))
                                        {

                                            //exeProcess.WaitForExit();
                                        }
                                    }
                                    catch
                                    {
                                        // If There is an error, keep the lime 
                                        //om next file
                                        sw.WriteLine(commandLine);
                                    }
                                }
                                
                            }
                        }
                    }
                    File.Delete(fi.FullName);
                    File.Move(tempFile, fi.FullName);
                    GrantAccess(fi.FullName);
                }
            }
        }

    }
}
